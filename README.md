# encycodepedia

## Project Outline

todo

## Getting started

1. Create a folder somewhere, where you would like to store this
   repository (and possibly more from us in the future).
2. Click on `Clone` on gitlab.com and copy the link.
3. Open a Terminal in the folder where you're working in and type:
   `git clone <LinkToRepositoryGoesHere>`
4. `cd` into the repository and contribute.

## Contribute

todo: outline

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Roadmap

todo

## Authors and acknowledgment

todo

## License

todo: agplv3

## Project status

Currently scaffolding
